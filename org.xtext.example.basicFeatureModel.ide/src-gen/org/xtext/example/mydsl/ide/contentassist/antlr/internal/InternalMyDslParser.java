package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Mandatory'", "'Optional'", "'Xor'", "'Or'", "'OrGroup'", "'XorGroup'", "'or'", "'and'", "'=>'", "'<=>'", "'Root'", "'Constraints'", "'['", "']'", "'{'", "'}'", "'~'", "'('", "')'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleRoot"
    // InternalMyDsl.g:53:1: entryRuleRoot : ruleRoot EOF ;
    public final void entryRuleRoot() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleRoot EOF )
            // InternalMyDsl.g:55:1: ruleRoot EOF
            {
             before(grammarAccess.getRootRule()); 
            pushFollow(FOLLOW_1);
            ruleRoot();

            state._fsp--;

             after(grammarAccess.getRootRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRoot"


    // $ANTLR start "ruleRoot"
    // InternalMyDsl.g:62:1: ruleRoot : ( ( rule__Root__Group__0 ) ) ;
    public final void ruleRoot() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Root__Group__0 ) ) )
            // InternalMyDsl.g:67:2: ( ( rule__Root__Group__0 ) )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Root__Group__0 ) )
            // InternalMyDsl.g:68:3: ( rule__Root__Group__0 )
            {
             before(grammarAccess.getRootAccess().getGroup()); 
            // InternalMyDsl.g:69:3: ( rule__Root__Group__0 )
            // InternalMyDsl.g:69:4: rule__Root__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Root__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRootAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRoot"


    // $ANTLR start "entryRuleNode"
    // InternalMyDsl.g:78:1: entryRuleNode : ruleNode EOF ;
    public final void entryRuleNode() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleNode EOF )
            // InternalMyDsl.g:80:1: ruleNode EOF
            {
             before(grammarAccess.getNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // InternalMyDsl.g:87:1: ruleNode : ( ( rule__Node__Alternatives ) ) ;
    public final void ruleNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Node__Alternatives ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Node__Alternatives ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Node__Alternatives ) )
            // InternalMyDsl.g:93:3: ( rule__Node__Alternatives )
            {
             before(grammarAccess.getNodeAccess().getAlternatives()); 
            // InternalMyDsl.g:94:3: ( rule__Node__Alternatives )
            // InternalMyDsl.g:94:4: rule__Node__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Node__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNodeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleUniNode"
    // InternalMyDsl.g:103:1: entryRuleUniNode : ruleUniNode EOF ;
    public final void entryRuleUniNode() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleUniNode EOF )
            // InternalMyDsl.g:105:1: ruleUniNode EOF
            {
             before(grammarAccess.getUniNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleUniNode();

            state._fsp--;

             after(grammarAccess.getUniNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUniNode"


    // $ANTLR start "ruleUniNode"
    // InternalMyDsl.g:112:1: ruleUniNode : ( ( rule__UniNode__Group__0 ) ) ;
    public final void ruleUniNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__UniNode__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__UniNode__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__UniNode__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__UniNode__Group__0 )
            {
             before(grammarAccess.getUniNodeAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__UniNode__Group__0 )
            // InternalMyDsl.g:119:4: rule__UniNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UniNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUniNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUniNode"


    // $ANTLR start "entryRuleNodeType"
    // InternalMyDsl.g:128:1: entryRuleNodeType : ruleNodeType EOF ;
    public final void entryRuleNodeType() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleNodeType EOF )
            // InternalMyDsl.g:130:1: ruleNodeType EOF
            {
             before(grammarAccess.getNodeTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleNodeType();

            state._fsp--;

             after(grammarAccess.getNodeTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNodeType"


    // $ANTLR start "ruleNodeType"
    // InternalMyDsl.g:137:1: ruleNodeType : ( ( rule__NodeType__Alternatives ) ) ;
    public final void ruleNodeType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__NodeType__Alternatives ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__NodeType__Alternatives ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__NodeType__Alternatives ) )
            // InternalMyDsl.g:143:3: ( rule__NodeType__Alternatives )
            {
             before(grammarAccess.getNodeTypeAccess().getAlternatives()); 
            // InternalMyDsl.g:144:3: ( rule__NodeType__Alternatives )
            // InternalMyDsl.g:144:4: rule__NodeType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__NodeType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getNodeTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNodeType"


    // $ANTLR start "entryRuleMultiNode"
    // InternalMyDsl.g:153:1: entryRuleMultiNode : ruleMultiNode EOF ;
    public final void entryRuleMultiNode() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleMultiNode EOF )
            // InternalMyDsl.g:155:1: ruleMultiNode EOF
            {
             before(grammarAccess.getMultiNodeRule()); 
            pushFollow(FOLLOW_1);
            ruleMultiNode();

            state._fsp--;

             after(grammarAccess.getMultiNodeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiNode"


    // $ANTLR start "ruleMultiNode"
    // InternalMyDsl.g:162:1: ruleMultiNode : ( ( rule__MultiNode__Group__0 ) ) ;
    public final void ruleMultiNode() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__MultiNode__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__MultiNode__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__MultiNode__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__MultiNode__Group__0 )
            {
             before(grammarAccess.getMultiNodeAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__MultiNode__Group__0 )
            // InternalMyDsl.g:169:4: rule__MultiNode__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MultiNode__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMultiNodeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiNode"


    // $ANTLR start "entryRuleMultiType"
    // InternalMyDsl.g:178:1: entryRuleMultiType : ruleMultiType EOF ;
    public final void entryRuleMultiType() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleMultiType EOF )
            // InternalMyDsl.g:180:1: ruleMultiType EOF
            {
             before(grammarAccess.getMultiTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleMultiType();

            state._fsp--;

             after(grammarAccess.getMultiTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiType"


    // $ANTLR start "ruleMultiType"
    // InternalMyDsl.g:187:1: ruleMultiType : ( ( rule__MultiType__Alternatives ) ) ;
    public final void ruleMultiType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__MultiType__Alternatives ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__MultiType__Alternatives ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__MultiType__Alternatives ) )
            // InternalMyDsl.g:193:3: ( rule__MultiType__Alternatives )
            {
             before(grammarAccess.getMultiTypeAccess().getAlternatives()); 
            // InternalMyDsl.g:194:3: ( rule__MultiType__Alternatives )
            // InternalMyDsl.g:194:4: rule__MultiType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__MultiType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getMultiTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiType"


    // $ANTLR start "entryRuleFormula"
    // InternalMyDsl.g:203:1: entryRuleFormula : ruleFormula EOF ;
    public final void entryRuleFormula() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleFormula EOF )
            // InternalMyDsl.g:205:1: ruleFormula EOF
            {
             before(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalMyDsl.g:212:1: ruleFormula : ( ( rule__Formula__Alternatives ) ) ;
    public final void ruleFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__Formula__Alternatives ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__Formula__Alternatives ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__Formula__Alternatives ) )
            // InternalMyDsl.g:218:3: ( rule__Formula__Alternatives )
            {
             before(grammarAccess.getFormulaAccess().getAlternatives()); 
            // InternalMyDsl.g:219:3: ( rule__Formula__Alternatives )
            // InternalMyDsl.g:219:4: rule__Formula__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleNeg"
    // InternalMyDsl.g:228:1: entryRuleNeg : ruleNeg EOF ;
    public final void entryRuleNeg() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( ruleNeg EOF )
            // InternalMyDsl.g:230:1: ruleNeg EOF
            {
             before(grammarAccess.getNegRule()); 
            pushFollow(FOLLOW_1);
            ruleNeg();

            state._fsp--;

             after(grammarAccess.getNegRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNeg"


    // $ANTLR start "ruleNeg"
    // InternalMyDsl.g:237:1: ruleNeg : ( ( rule__Neg__Group__0 ) ) ;
    public final void ruleNeg() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__Neg__Group__0 ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__Neg__Group__0 ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__Neg__Group__0 ) )
            // InternalMyDsl.g:243:3: ( rule__Neg__Group__0 )
            {
             before(grammarAccess.getNegAccess().getGroup()); 
            // InternalMyDsl.g:244:3: ( rule__Neg__Group__0 )
            // InternalMyDsl.g:244:4: rule__Neg__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Neg__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNegAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNeg"


    // $ANTLR start "entryRuleBinary"
    // InternalMyDsl.g:253:1: entryRuleBinary : ruleBinary EOF ;
    public final void entryRuleBinary() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( ruleBinary EOF )
            // InternalMyDsl.g:255:1: ruleBinary EOF
            {
             before(grammarAccess.getBinaryRule()); 
            pushFollow(FOLLOW_1);
            ruleBinary();

            state._fsp--;

             after(grammarAccess.getBinaryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBinary"


    // $ANTLR start "ruleBinary"
    // InternalMyDsl.g:262:1: ruleBinary : ( ( rule__Binary__Group__0 ) ) ;
    public final void ruleBinary() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__Binary__Group__0 ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__Binary__Group__0 ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__Binary__Group__0 ) )
            // InternalMyDsl.g:268:3: ( rule__Binary__Group__0 )
            {
             before(grammarAccess.getBinaryAccess().getGroup()); 
            // InternalMyDsl.g:269:3: ( rule__Binary__Group__0 )
            // InternalMyDsl.g:269:4: rule__Binary__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Binary__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBinary"


    // $ANTLR start "entryRuleOp"
    // InternalMyDsl.g:278:1: entryRuleOp : ruleOp EOF ;
    public final void entryRuleOp() throws RecognitionException {
        try {
            // InternalMyDsl.g:279:1: ( ruleOp EOF )
            // InternalMyDsl.g:280:1: ruleOp EOF
            {
             before(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getOpRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalMyDsl.g:287:1: ruleOp : ( ( rule__Op__Alternatives ) ) ;
    public final void ruleOp() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:291:2: ( ( ( rule__Op__Alternatives ) ) )
            // InternalMyDsl.g:292:2: ( ( rule__Op__Alternatives ) )
            {
            // InternalMyDsl.g:292:2: ( ( rule__Op__Alternatives ) )
            // InternalMyDsl.g:293:3: ( rule__Op__Alternatives )
            {
             before(grammarAccess.getOpAccess().getAlternatives()); 
            // InternalMyDsl.g:294:3: ( rule__Op__Alternatives )
            // InternalMyDsl.g:294:4: rule__Op__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Op__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getOpAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOp"


    // $ANTLR start "rule__Node__Alternatives"
    // InternalMyDsl.g:302:1: rule__Node__Alternatives : ( ( ruleUniNode ) | ( ruleMultiNode ) );
    public final void rule__Node__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:306:1: ( ( ruleUniNode ) | ( ruleMultiNode ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( ((LA1_0>=11 && LA1_0<=14)) ) {
                alt1=1;
            }
            else if ( ((LA1_0>=15 && LA1_0<=16)) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalMyDsl.g:307:2: ( ruleUniNode )
                    {
                    // InternalMyDsl.g:307:2: ( ruleUniNode )
                    // InternalMyDsl.g:308:3: ruleUniNode
                    {
                     before(grammarAccess.getNodeAccess().getUniNodeParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleUniNode();

                    state._fsp--;

                     after(grammarAccess.getNodeAccess().getUniNodeParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:313:2: ( ruleMultiNode )
                    {
                    // InternalMyDsl.g:313:2: ( ruleMultiNode )
                    // InternalMyDsl.g:314:3: ruleMultiNode
                    {
                     before(grammarAccess.getNodeAccess().getMultiNodeParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleMultiNode();

                    state._fsp--;

                     after(grammarAccess.getNodeAccess().getMultiNodeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Node__Alternatives"


    // $ANTLR start "rule__NodeType__Alternatives"
    // InternalMyDsl.g:323:1: rule__NodeType__Alternatives : ( ( 'Mandatory' ) | ( 'Optional' ) | ( 'Xor' ) | ( 'Or' ) );
    public final void rule__NodeType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:327:1: ( ( 'Mandatory' ) | ( 'Optional' ) | ( 'Xor' ) | ( 'Or' ) )
            int alt2=4;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 12:
                {
                alt2=2;
                }
                break;
            case 13:
                {
                alt2=3;
                }
                break;
            case 14:
                {
                alt2=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:328:2: ( 'Mandatory' )
                    {
                    // InternalMyDsl.g:328:2: ( 'Mandatory' )
                    // InternalMyDsl.g:329:3: 'Mandatory'
                    {
                     before(grammarAccess.getNodeTypeAccess().getMandatoryKeyword_0()); 
                    match(input,11,FOLLOW_2); 
                     after(grammarAccess.getNodeTypeAccess().getMandatoryKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:334:2: ( 'Optional' )
                    {
                    // InternalMyDsl.g:334:2: ( 'Optional' )
                    // InternalMyDsl.g:335:3: 'Optional'
                    {
                     before(grammarAccess.getNodeTypeAccess().getOptionalKeyword_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getNodeTypeAccess().getOptionalKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:340:2: ( 'Xor' )
                    {
                    // InternalMyDsl.g:340:2: ( 'Xor' )
                    // InternalMyDsl.g:341:3: 'Xor'
                    {
                     before(grammarAccess.getNodeTypeAccess().getXorKeyword_2()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getNodeTypeAccess().getXorKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:346:2: ( 'Or' )
                    {
                    // InternalMyDsl.g:346:2: ( 'Or' )
                    // InternalMyDsl.g:347:3: 'Or'
                    {
                     before(grammarAccess.getNodeTypeAccess().getOrKeyword_3()); 
                    match(input,14,FOLLOW_2); 
                     after(grammarAccess.getNodeTypeAccess().getOrKeyword_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NodeType__Alternatives"


    // $ANTLR start "rule__MultiType__Alternatives"
    // InternalMyDsl.g:356:1: rule__MultiType__Alternatives : ( ( 'OrGroup' ) | ( 'XorGroup' ) );
    public final void rule__MultiType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:360:1: ( ( 'OrGroup' ) | ( 'XorGroup' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==15) ) {
                alt3=1;
            }
            else if ( (LA3_0==16) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:361:2: ( 'OrGroup' )
                    {
                    // InternalMyDsl.g:361:2: ( 'OrGroup' )
                    // InternalMyDsl.g:362:3: 'OrGroup'
                    {
                     before(grammarAccess.getMultiTypeAccess().getOrGroupKeyword_0()); 
                    match(input,15,FOLLOW_2); 
                     after(grammarAccess.getMultiTypeAccess().getOrGroupKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:367:2: ( 'XorGroup' )
                    {
                    // InternalMyDsl.g:367:2: ( 'XorGroup' )
                    // InternalMyDsl.g:368:3: 'XorGroup'
                    {
                     before(grammarAccess.getMultiTypeAccess().getXorGroupKeyword_1()); 
                    match(input,16,FOLLOW_2); 
                     after(grammarAccess.getMultiTypeAccess().getXorGroupKeyword_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiType__Alternatives"


    // $ANTLR start "rule__Formula__Alternatives"
    // InternalMyDsl.g:377:1: rule__Formula__Alternatives : ( ( ( rule__Formula__FeatureAssignment_0 ) ) | ( ruleNeg ) | ( ruleBinary ) );
    public final void rule__Formula__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:381:1: ( ( ( rule__Formula__FeatureAssignment_0 ) ) | ( ruleNeg ) | ( ruleBinary ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt4=1;
                }
                break;
            case 27:
                {
                alt4=2;
                }
                break;
            case 28:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:382:2: ( ( rule__Formula__FeatureAssignment_0 ) )
                    {
                    // InternalMyDsl.g:382:2: ( ( rule__Formula__FeatureAssignment_0 ) )
                    // InternalMyDsl.g:383:3: ( rule__Formula__FeatureAssignment_0 )
                    {
                     before(grammarAccess.getFormulaAccess().getFeatureAssignment_0()); 
                    // InternalMyDsl.g:384:3: ( rule__Formula__FeatureAssignment_0 )
                    // InternalMyDsl.g:384:4: rule__Formula__FeatureAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Formula__FeatureAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFormulaAccess().getFeatureAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:388:2: ( ruleNeg )
                    {
                    // InternalMyDsl.g:388:2: ( ruleNeg )
                    // InternalMyDsl.g:389:3: ruleNeg
                    {
                     before(grammarAccess.getFormulaAccess().getNegParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleNeg();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getNegParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:394:2: ( ruleBinary )
                    {
                    // InternalMyDsl.g:394:2: ( ruleBinary )
                    // InternalMyDsl.g:395:3: ruleBinary
                    {
                     before(grammarAccess.getFormulaAccess().getBinaryParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleBinary();

                    state._fsp--;

                     after(grammarAccess.getFormulaAccess().getBinaryParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Alternatives"


    // $ANTLR start "rule__Op__Alternatives"
    // InternalMyDsl.g:404:1: rule__Op__Alternatives : ( ( 'or' ) | ( 'and' ) | ( '=>' ) | ( '<=>' ) );
    public final void rule__Op__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:408:1: ( ( 'or' ) | ( 'and' ) | ( '=>' ) | ( '<=>' ) )
            int alt5=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt5=1;
                }
                break;
            case 18:
                {
                alt5=2;
                }
                break;
            case 19:
                {
                alt5=3;
                }
                break;
            case 20:
                {
                alt5=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:409:2: ( 'or' )
                    {
                    // InternalMyDsl.g:409:2: ( 'or' )
                    // InternalMyDsl.g:410:3: 'or'
                    {
                     before(grammarAccess.getOpAccess().getOrKeyword_0()); 
                    match(input,17,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getOrKeyword_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:415:2: ( 'and' )
                    {
                    // InternalMyDsl.g:415:2: ( 'and' )
                    // InternalMyDsl.g:416:3: 'and'
                    {
                     before(grammarAccess.getOpAccess().getAndKeyword_1()); 
                    match(input,18,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getAndKeyword_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:421:2: ( '=>' )
                    {
                    // InternalMyDsl.g:421:2: ( '=>' )
                    // InternalMyDsl.g:422:3: '=>'
                    {
                     before(grammarAccess.getOpAccess().getEqualsSignGreaterThanSignKeyword_2()); 
                    match(input,19,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getEqualsSignGreaterThanSignKeyword_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:427:2: ( '<=>' )
                    {
                    // InternalMyDsl.g:427:2: ( '<=>' )
                    // InternalMyDsl.g:428:3: '<=>'
                    {
                     before(grammarAccess.getOpAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_3()); 
                    match(input,20,FOLLOW_2); 
                     after(grammarAccess.getOpAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Op__Alternatives"


    // $ANTLR start "rule__Root__Group__0"
    // InternalMyDsl.g:437:1: rule__Root__Group__0 : rule__Root__Group__0__Impl rule__Root__Group__1 ;
    public final void rule__Root__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:441:1: ( rule__Root__Group__0__Impl rule__Root__Group__1 )
            // InternalMyDsl.g:442:2: rule__Root__Group__0__Impl rule__Root__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Root__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__0"


    // $ANTLR start "rule__Root__Group__0__Impl"
    // InternalMyDsl.g:449:1: rule__Root__Group__0__Impl : ( 'Root' ) ;
    public final void rule__Root__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:453:1: ( ( 'Root' ) )
            // InternalMyDsl.g:454:1: ( 'Root' )
            {
            // InternalMyDsl.g:454:1: ( 'Root' )
            // InternalMyDsl.g:455:2: 'Root'
            {
             before(grammarAccess.getRootAccess().getRootKeyword_0()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getRootKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__0__Impl"


    // $ANTLR start "rule__Root__Group__1"
    // InternalMyDsl.g:464:1: rule__Root__Group__1 : rule__Root__Group__1__Impl rule__Root__Group__2 ;
    public final void rule__Root__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:468:1: ( rule__Root__Group__1__Impl rule__Root__Group__2 )
            // InternalMyDsl.g:469:2: rule__Root__Group__1__Impl rule__Root__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Root__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__1"


    // $ANTLR start "rule__Root__Group__1__Impl"
    // InternalMyDsl.g:476:1: rule__Root__Group__1__Impl : ( ( rule__Root__NameAssignment_1 ) ) ;
    public final void rule__Root__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:480:1: ( ( ( rule__Root__NameAssignment_1 ) ) )
            // InternalMyDsl.g:481:1: ( ( rule__Root__NameAssignment_1 ) )
            {
            // InternalMyDsl.g:481:1: ( ( rule__Root__NameAssignment_1 ) )
            // InternalMyDsl.g:482:2: ( rule__Root__NameAssignment_1 )
            {
             before(grammarAccess.getRootAccess().getNameAssignment_1()); 
            // InternalMyDsl.g:483:2: ( rule__Root__NameAssignment_1 )
            // InternalMyDsl.g:483:3: rule__Root__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Root__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRootAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__1__Impl"


    // $ANTLR start "rule__Root__Group__2"
    // InternalMyDsl.g:491:1: rule__Root__Group__2 : rule__Root__Group__2__Impl rule__Root__Group__3 ;
    public final void rule__Root__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:495:1: ( rule__Root__Group__2__Impl rule__Root__Group__3 )
            // InternalMyDsl.g:496:2: rule__Root__Group__2__Impl rule__Root__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Root__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__2"


    // $ANTLR start "rule__Root__Group__2__Impl"
    // InternalMyDsl.g:503:1: rule__Root__Group__2__Impl : ( ( rule__Root__Group_2__0 )? ) ;
    public final void rule__Root__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:507:1: ( ( ( rule__Root__Group_2__0 )? ) )
            // InternalMyDsl.g:508:1: ( ( rule__Root__Group_2__0 )? )
            {
            // InternalMyDsl.g:508:1: ( ( rule__Root__Group_2__0 )? )
            // InternalMyDsl.g:509:2: ( rule__Root__Group_2__0 )?
            {
             before(grammarAccess.getRootAccess().getGroup_2()); 
            // InternalMyDsl.g:510:2: ( rule__Root__Group_2__0 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==25) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:510:3: rule__Root__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Root__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRootAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__2__Impl"


    // $ANTLR start "rule__Root__Group__3"
    // InternalMyDsl.g:518:1: rule__Root__Group__3 : rule__Root__Group__3__Impl rule__Root__Group__4 ;
    public final void rule__Root__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:522:1: ( rule__Root__Group__3__Impl rule__Root__Group__4 )
            // InternalMyDsl.g:523:2: rule__Root__Group__3__Impl rule__Root__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__Root__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__3"


    // $ANTLR start "rule__Root__Group__3__Impl"
    // InternalMyDsl.g:530:1: rule__Root__Group__3__Impl : ( 'Constraints' ) ;
    public final void rule__Root__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:534:1: ( ( 'Constraints' ) )
            // InternalMyDsl.g:535:1: ( 'Constraints' )
            {
            // InternalMyDsl.g:535:1: ( 'Constraints' )
            // InternalMyDsl.g:536:2: 'Constraints'
            {
             before(grammarAccess.getRootAccess().getConstraintsKeyword_3()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getConstraintsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__3__Impl"


    // $ANTLR start "rule__Root__Group__4"
    // InternalMyDsl.g:545:1: rule__Root__Group__4 : rule__Root__Group__4__Impl rule__Root__Group__5 ;
    public final void rule__Root__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:549:1: ( rule__Root__Group__4__Impl rule__Root__Group__5 )
            // InternalMyDsl.g:550:2: rule__Root__Group__4__Impl rule__Root__Group__5
            {
            pushFollow(FOLLOW_6);
            rule__Root__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__4"


    // $ANTLR start "rule__Root__Group__4__Impl"
    // InternalMyDsl.g:557:1: rule__Root__Group__4__Impl : ( '[' ) ;
    public final void rule__Root__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:561:1: ( ( '[' ) )
            // InternalMyDsl.g:562:1: ( '[' )
            {
            // InternalMyDsl.g:562:1: ( '[' )
            // InternalMyDsl.g:563:2: '['
            {
             before(grammarAccess.getRootAccess().getLeftSquareBracketKeyword_4()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getLeftSquareBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__4__Impl"


    // $ANTLR start "rule__Root__Group__5"
    // InternalMyDsl.g:572:1: rule__Root__Group__5 : rule__Root__Group__5__Impl rule__Root__Group__6 ;
    public final void rule__Root__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:576:1: ( rule__Root__Group__5__Impl rule__Root__Group__6 )
            // InternalMyDsl.g:577:2: rule__Root__Group__5__Impl rule__Root__Group__6
            {
            pushFollow(FOLLOW_6);
            rule__Root__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__5"


    // $ANTLR start "rule__Root__Group__5__Impl"
    // InternalMyDsl.g:584:1: rule__Root__Group__5__Impl : ( ( rule__Root__ConstraintAssignment_5 )* ) ;
    public final void rule__Root__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:588:1: ( ( ( rule__Root__ConstraintAssignment_5 )* ) )
            // InternalMyDsl.g:589:1: ( ( rule__Root__ConstraintAssignment_5 )* )
            {
            // InternalMyDsl.g:589:1: ( ( rule__Root__ConstraintAssignment_5 )* )
            // InternalMyDsl.g:590:2: ( rule__Root__ConstraintAssignment_5 )*
            {
             before(grammarAccess.getRootAccess().getConstraintAssignment_5()); 
            // InternalMyDsl.g:591:2: ( rule__Root__ConstraintAssignment_5 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID||(LA7_0>=27 && LA7_0<=28)) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:591:3: rule__Root__ConstraintAssignment_5
            	    {
            	    pushFollow(FOLLOW_7);
            	    rule__Root__ConstraintAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getRootAccess().getConstraintAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__5__Impl"


    // $ANTLR start "rule__Root__Group__6"
    // InternalMyDsl.g:599:1: rule__Root__Group__6 : rule__Root__Group__6__Impl ;
    public final void rule__Root__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:603:1: ( rule__Root__Group__6__Impl )
            // InternalMyDsl.g:604:2: rule__Root__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Root__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__6"


    // $ANTLR start "rule__Root__Group__6__Impl"
    // InternalMyDsl.g:610:1: rule__Root__Group__6__Impl : ( ']' ) ;
    public final void rule__Root__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:614:1: ( ( ']' ) )
            // InternalMyDsl.g:615:1: ( ']' )
            {
            // InternalMyDsl.g:615:1: ( ']' )
            // InternalMyDsl.g:616:2: ']'
            {
             before(grammarAccess.getRootAccess().getRightSquareBracketKeyword_6()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getRightSquareBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group__6__Impl"


    // $ANTLR start "rule__Root__Group_2__0"
    // InternalMyDsl.g:626:1: rule__Root__Group_2__0 : rule__Root__Group_2__0__Impl rule__Root__Group_2__1 ;
    public final void rule__Root__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:630:1: ( rule__Root__Group_2__0__Impl rule__Root__Group_2__1 )
            // InternalMyDsl.g:631:2: rule__Root__Group_2__0__Impl rule__Root__Group_2__1
            {
            pushFollow(FOLLOW_8);
            rule__Root__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group_2__0"


    // $ANTLR start "rule__Root__Group_2__0__Impl"
    // InternalMyDsl.g:638:1: rule__Root__Group_2__0__Impl : ( '{' ) ;
    public final void rule__Root__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:642:1: ( ( '{' ) )
            // InternalMyDsl.g:643:1: ( '{' )
            {
            // InternalMyDsl.g:643:1: ( '{' )
            // InternalMyDsl.g:644:2: '{'
            {
             before(grammarAccess.getRootAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group_2__0__Impl"


    // $ANTLR start "rule__Root__Group_2__1"
    // InternalMyDsl.g:653:1: rule__Root__Group_2__1 : rule__Root__Group_2__1__Impl rule__Root__Group_2__2 ;
    public final void rule__Root__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:657:1: ( rule__Root__Group_2__1__Impl rule__Root__Group_2__2 )
            // InternalMyDsl.g:658:2: rule__Root__Group_2__1__Impl rule__Root__Group_2__2
            {
            pushFollow(FOLLOW_8);
            rule__Root__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Root__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group_2__1"


    // $ANTLR start "rule__Root__Group_2__1__Impl"
    // InternalMyDsl.g:665:1: rule__Root__Group_2__1__Impl : ( ( rule__Root__SonsAssignment_2_1 )* ) ;
    public final void rule__Root__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:669:1: ( ( ( rule__Root__SonsAssignment_2_1 )* ) )
            // InternalMyDsl.g:670:1: ( ( rule__Root__SonsAssignment_2_1 )* )
            {
            // InternalMyDsl.g:670:1: ( ( rule__Root__SonsAssignment_2_1 )* )
            // InternalMyDsl.g:671:2: ( rule__Root__SonsAssignment_2_1 )*
            {
             before(grammarAccess.getRootAccess().getSonsAssignment_2_1()); 
            // InternalMyDsl.g:672:2: ( rule__Root__SonsAssignment_2_1 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=11 && LA8_0<=16)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:672:3: rule__Root__SonsAssignment_2_1
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Root__SonsAssignment_2_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getRootAccess().getSonsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group_2__1__Impl"


    // $ANTLR start "rule__Root__Group_2__2"
    // InternalMyDsl.g:680:1: rule__Root__Group_2__2 : rule__Root__Group_2__2__Impl ;
    public final void rule__Root__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:684:1: ( rule__Root__Group_2__2__Impl )
            // InternalMyDsl.g:685:2: rule__Root__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Root__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group_2__2"


    // $ANTLR start "rule__Root__Group_2__2__Impl"
    // InternalMyDsl.g:691:1: rule__Root__Group_2__2__Impl : ( '}' ) ;
    public final void rule__Root__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:695:1: ( ( '}' ) )
            // InternalMyDsl.g:696:1: ( '}' )
            {
            // InternalMyDsl.g:696:1: ( '}' )
            // InternalMyDsl.g:697:2: '}'
            {
             before(grammarAccess.getRootAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__Group_2__2__Impl"


    // $ANTLR start "rule__UniNode__Group__0"
    // InternalMyDsl.g:707:1: rule__UniNode__Group__0 : rule__UniNode__Group__0__Impl rule__UniNode__Group__1 ;
    public final void rule__UniNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:711:1: ( rule__UniNode__Group__0__Impl rule__UniNode__Group__1 )
            // InternalMyDsl.g:712:2: rule__UniNode__Group__0__Impl rule__UniNode__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__UniNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UniNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group__0"


    // $ANTLR start "rule__UniNode__Group__0__Impl"
    // InternalMyDsl.g:719:1: rule__UniNode__Group__0__Impl : ( ( rule__UniNode__TypeAssignment_0 ) ) ;
    public final void rule__UniNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:723:1: ( ( ( rule__UniNode__TypeAssignment_0 ) ) )
            // InternalMyDsl.g:724:1: ( ( rule__UniNode__TypeAssignment_0 ) )
            {
            // InternalMyDsl.g:724:1: ( ( rule__UniNode__TypeAssignment_0 ) )
            // InternalMyDsl.g:725:2: ( rule__UniNode__TypeAssignment_0 )
            {
             before(grammarAccess.getUniNodeAccess().getTypeAssignment_0()); 
            // InternalMyDsl.g:726:2: ( rule__UniNode__TypeAssignment_0 )
            // InternalMyDsl.g:726:3: rule__UniNode__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__UniNode__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getUniNodeAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group__0__Impl"


    // $ANTLR start "rule__UniNode__Group__1"
    // InternalMyDsl.g:734:1: rule__UniNode__Group__1 : rule__UniNode__Group__1__Impl rule__UniNode__Group__2 ;
    public final void rule__UniNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:738:1: ( rule__UniNode__Group__1__Impl rule__UniNode__Group__2 )
            // InternalMyDsl.g:739:2: rule__UniNode__Group__1__Impl rule__UniNode__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__UniNode__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UniNode__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group__1"


    // $ANTLR start "rule__UniNode__Group__1__Impl"
    // InternalMyDsl.g:746:1: rule__UniNode__Group__1__Impl : ( ( rule__UniNode__NameAssignment_1 ) ) ;
    public final void rule__UniNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:750:1: ( ( ( rule__UniNode__NameAssignment_1 ) ) )
            // InternalMyDsl.g:751:1: ( ( rule__UniNode__NameAssignment_1 ) )
            {
            // InternalMyDsl.g:751:1: ( ( rule__UniNode__NameAssignment_1 ) )
            // InternalMyDsl.g:752:2: ( rule__UniNode__NameAssignment_1 )
            {
             before(grammarAccess.getUniNodeAccess().getNameAssignment_1()); 
            // InternalMyDsl.g:753:2: ( rule__UniNode__NameAssignment_1 )
            // InternalMyDsl.g:753:3: rule__UniNode__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UniNode__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUniNodeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group__1__Impl"


    // $ANTLR start "rule__UniNode__Group__2"
    // InternalMyDsl.g:761:1: rule__UniNode__Group__2 : rule__UniNode__Group__2__Impl ;
    public final void rule__UniNode__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:765:1: ( rule__UniNode__Group__2__Impl )
            // InternalMyDsl.g:766:2: rule__UniNode__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UniNode__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group__2"


    // $ANTLR start "rule__UniNode__Group__2__Impl"
    // InternalMyDsl.g:772:1: rule__UniNode__Group__2__Impl : ( ( rule__UniNode__Group_2__0 )? ) ;
    public final void rule__UniNode__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:776:1: ( ( ( rule__UniNode__Group_2__0 )? ) )
            // InternalMyDsl.g:777:1: ( ( rule__UniNode__Group_2__0 )? )
            {
            // InternalMyDsl.g:777:1: ( ( rule__UniNode__Group_2__0 )? )
            // InternalMyDsl.g:778:2: ( rule__UniNode__Group_2__0 )?
            {
             before(grammarAccess.getUniNodeAccess().getGroup_2()); 
            // InternalMyDsl.g:779:2: ( rule__UniNode__Group_2__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==25) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:779:3: rule__UniNode__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__UniNode__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getUniNodeAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group__2__Impl"


    // $ANTLR start "rule__UniNode__Group_2__0"
    // InternalMyDsl.g:788:1: rule__UniNode__Group_2__0 : rule__UniNode__Group_2__0__Impl rule__UniNode__Group_2__1 ;
    public final void rule__UniNode__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:792:1: ( rule__UniNode__Group_2__0__Impl rule__UniNode__Group_2__1 )
            // InternalMyDsl.g:793:2: rule__UniNode__Group_2__0__Impl rule__UniNode__Group_2__1
            {
            pushFollow(FOLLOW_8);
            rule__UniNode__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UniNode__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group_2__0"


    // $ANTLR start "rule__UniNode__Group_2__0__Impl"
    // InternalMyDsl.g:800:1: rule__UniNode__Group_2__0__Impl : ( '{' ) ;
    public final void rule__UniNode__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:804:1: ( ( '{' ) )
            // InternalMyDsl.g:805:1: ( '{' )
            {
            // InternalMyDsl.g:805:1: ( '{' )
            // InternalMyDsl.g:806:2: '{'
            {
             before(grammarAccess.getUniNodeAccess().getLeftCurlyBracketKeyword_2_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getUniNodeAccess().getLeftCurlyBracketKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group_2__0__Impl"


    // $ANTLR start "rule__UniNode__Group_2__1"
    // InternalMyDsl.g:815:1: rule__UniNode__Group_2__1 : rule__UniNode__Group_2__1__Impl rule__UniNode__Group_2__2 ;
    public final void rule__UniNode__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:819:1: ( rule__UniNode__Group_2__1__Impl rule__UniNode__Group_2__2 )
            // InternalMyDsl.g:820:2: rule__UniNode__Group_2__1__Impl rule__UniNode__Group_2__2
            {
            pushFollow(FOLLOW_8);
            rule__UniNode__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UniNode__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group_2__1"


    // $ANTLR start "rule__UniNode__Group_2__1__Impl"
    // InternalMyDsl.g:827:1: rule__UniNode__Group_2__1__Impl : ( ( rule__UniNode__SonsAssignment_2_1 )* ) ;
    public final void rule__UniNode__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:831:1: ( ( ( rule__UniNode__SonsAssignment_2_1 )* ) )
            // InternalMyDsl.g:832:1: ( ( rule__UniNode__SonsAssignment_2_1 )* )
            {
            // InternalMyDsl.g:832:1: ( ( rule__UniNode__SonsAssignment_2_1 )* )
            // InternalMyDsl.g:833:2: ( rule__UniNode__SonsAssignment_2_1 )*
            {
             before(grammarAccess.getUniNodeAccess().getSonsAssignment_2_1()); 
            // InternalMyDsl.g:834:2: ( rule__UniNode__SonsAssignment_2_1 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>=11 && LA10_0<=16)) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:834:3: rule__UniNode__SonsAssignment_2_1
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__UniNode__SonsAssignment_2_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getUniNodeAccess().getSonsAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group_2__1__Impl"


    // $ANTLR start "rule__UniNode__Group_2__2"
    // InternalMyDsl.g:842:1: rule__UniNode__Group_2__2 : rule__UniNode__Group_2__2__Impl ;
    public final void rule__UniNode__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:846:1: ( rule__UniNode__Group_2__2__Impl )
            // InternalMyDsl.g:847:2: rule__UniNode__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UniNode__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group_2__2"


    // $ANTLR start "rule__UniNode__Group_2__2__Impl"
    // InternalMyDsl.g:853:1: rule__UniNode__Group_2__2__Impl : ( '}' ) ;
    public final void rule__UniNode__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:857:1: ( ( '}' ) )
            // InternalMyDsl.g:858:1: ( '}' )
            {
            // InternalMyDsl.g:858:1: ( '}' )
            // InternalMyDsl.g:859:2: '}'
            {
             before(grammarAccess.getUniNodeAccess().getRightCurlyBracketKeyword_2_2()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getUniNodeAccess().getRightCurlyBracketKeyword_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__Group_2__2__Impl"


    // $ANTLR start "rule__MultiNode__Group__0"
    // InternalMyDsl.g:869:1: rule__MultiNode__Group__0 : rule__MultiNode__Group__0__Impl rule__MultiNode__Group__1 ;
    public final void rule__MultiNode__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:873:1: ( rule__MultiNode__Group__0__Impl rule__MultiNode__Group__1 )
            // InternalMyDsl.g:874:2: rule__MultiNode__Group__0__Impl rule__MultiNode__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__MultiNode__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiNode__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__0"


    // $ANTLR start "rule__MultiNode__Group__0__Impl"
    // InternalMyDsl.g:881:1: rule__MultiNode__Group__0__Impl : ( ( rule__MultiNode__TypeAssignment_0 ) ) ;
    public final void rule__MultiNode__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:885:1: ( ( ( rule__MultiNode__TypeAssignment_0 ) ) )
            // InternalMyDsl.g:886:1: ( ( rule__MultiNode__TypeAssignment_0 ) )
            {
            // InternalMyDsl.g:886:1: ( ( rule__MultiNode__TypeAssignment_0 ) )
            // InternalMyDsl.g:887:2: ( rule__MultiNode__TypeAssignment_0 )
            {
             before(grammarAccess.getMultiNodeAccess().getTypeAssignment_0()); 
            // InternalMyDsl.g:888:2: ( rule__MultiNode__TypeAssignment_0 )
            // InternalMyDsl.g:888:3: rule__MultiNode__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__MultiNode__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getMultiNodeAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__0__Impl"


    // $ANTLR start "rule__MultiNode__Group__1"
    // InternalMyDsl.g:896:1: rule__MultiNode__Group__1 : rule__MultiNode__Group__1__Impl rule__MultiNode__Group__2 ;
    public final void rule__MultiNode__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:900:1: ( rule__MultiNode__Group__1__Impl rule__MultiNode__Group__2 )
            // InternalMyDsl.g:901:2: rule__MultiNode__Group__1__Impl rule__MultiNode__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__MultiNode__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiNode__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__1"


    // $ANTLR start "rule__MultiNode__Group__1__Impl"
    // InternalMyDsl.g:908:1: rule__MultiNode__Group__1__Impl : ( '{' ) ;
    public final void rule__MultiNode__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:912:1: ( ( '{' ) )
            // InternalMyDsl.g:913:1: ( '{' )
            {
            // InternalMyDsl.g:913:1: ( '{' )
            // InternalMyDsl.g:914:2: '{'
            {
             before(grammarAccess.getMultiNodeAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getMultiNodeAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__1__Impl"


    // $ANTLR start "rule__MultiNode__Group__2"
    // InternalMyDsl.g:923:1: rule__MultiNode__Group__2 : rule__MultiNode__Group__2__Impl rule__MultiNode__Group__3 ;
    public final void rule__MultiNode__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:927:1: ( rule__MultiNode__Group__2__Impl rule__MultiNode__Group__3 )
            // InternalMyDsl.g:928:2: rule__MultiNode__Group__2__Impl rule__MultiNode__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__MultiNode__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiNode__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__2"


    // $ANTLR start "rule__MultiNode__Group__2__Impl"
    // InternalMyDsl.g:935:1: rule__MultiNode__Group__2__Impl : ( ( ( rule__MultiNode__SonsAssignment_2 ) ) ( ( rule__MultiNode__SonsAssignment_2 )* ) ) ;
    public final void rule__MultiNode__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:939:1: ( ( ( ( rule__MultiNode__SonsAssignment_2 ) ) ( ( rule__MultiNode__SonsAssignment_2 )* ) ) )
            // InternalMyDsl.g:940:1: ( ( ( rule__MultiNode__SonsAssignment_2 ) ) ( ( rule__MultiNode__SonsAssignment_2 )* ) )
            {
            // InternalMyDsl.g:940:1: ( ( ( rule__MultiNode__SonsAssignment_2 ) ) ( ( rule__MultiNode__SonsAssignment_2 )* ) )
            // InternalMyDsl.g:941:2: ( ( rule__MultiNode__SonsAssignment_2 ) ) ( ( rule__MultiNode__SonsAssignment_2 )* )
            {
            // InternalMyDsl.g:941:2: ( ( rule__MultiNode__SonsAssignment_2 ) )
            // InternalMyDsl.g:942:3: ( rule__MultiNode__SonsAssignment_2 )
            {
             before(grammarAccess.getMultiNodeAccess().getSonsAssignment_2()); 
            // InternalMyDsl.g:943:3: ( rule__MultiNode__SonsAssignment_2 )
            // InternalMyDsl.g:943:4: rule__MultiNode__SonsAssignment_2
            {
            pushFollow(FOLLOW_9);
            rule__MultiNode__SonsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMultiNodeAccess().getSonsAssignment_2()); 

            }

            // InternalMyDsl.g:946:2: ( ( rule__MultiNode__SonsAssignment_2 )* )
            // InternalMyDsl.g:947:3: ( rule__MultiNode__SonsAssignment_2 )*
            {
             before(grammarAccess.getMultiNodeAccess().getSonsAssignment_2()); 
            // InternalMyDsl.g:948:3: ( rule__MultiNode__SonsAssignment_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>=11 && LA11_0<=16)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMyDsl.g:948:4: rule__MultiNode__SonsAssignment_2
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__MultiNode__SonsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getMultiNodeAccess().getSonsAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__2__Impl"


    // $ANTLR start "rule__MultiNode__Group__3"
    // InternalMyDsl.g:957:1: rule__MultiNode__Group__3 : rule__MultiNode__Group__3__Impl ;
    public final void rule__MultiNode__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:961:1: ( rule__MultiNode__Group__3__Impl )
            // InternalMyDsl.g:962:2: rule__MultiNode__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MultiNode__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__3"


    // $ANTLR start "rule__MultiNode__Group__3__Impl"
    // InternalMyDsl.g:968:1: rule__MultiNode__Group__3__Impl : ( '}' ) ;
    public final void rule__MultiNode__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:972:1: ( ( '}' ) )
            // InternalMyDsl.g:973:1: ( '}' )
            {
            // InternalMyDsl.g:973:1: ( '}' )
            // InternalMyDsl.g:974:2: '}'
            {
             before(grammarAccess.getMultiNodeAccess().getRightCurlyBracketKeyword_3()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getMultiNodeAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__Group__3__Impl"


    // $ANTLR start "rule__Neg__Group__0"
    // InternalMyDsl.g:984:1: rule__Neg__Group__0 : rule__Neg__Group__0__Impl rule__Neg__Group__1 ;
    public final void rule__Neg__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:988:1: ( rule__Neg__Group__0__Impl rule__Neg__Group__1 )
            // InternalMyDsl.g:989:2: rule__Neg__Group__0__Impl rule__Neg__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Neg__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Neg__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__0"


    // $ANTLR start "rule__Neg__Group__0__Impl"
    // InternalMyDsl.g:996:1: rule__Neg__Group__0__Impl : ( '~' ) ;
    public final void rule__Neg__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1000:1: ( ( '~' ) )
            // InternalMyDsl.g:1001:1: ( '~' )
            {
            // InternalMyDsl.g:1001:1: ( '~' )
            // InternalMyDsl.g:1002:2: '~'
            {
             before(grammarAccess.getNegAccess().getTildeKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getNegAccess().getTildeKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__0__Impl"


    // $ANTLR start "rule__Neg__Group__1"
    // InternalMyDsl.g:1011:1: rule__Neg__Group__1 : rule__Neg__Group__1__Impl ;
    public final void rule__Neg__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1015:1: ( rule__Neg__Group__1__Impl )
            // InternalMyDsl.g:1016:2: rule__Neg__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Neg__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__1"


    // $ANTLR start "rule__Neg__Group__1__Impl"
    // InternalMyDsl.g:1022:1: rule__Neg__Group__1__Impl : ( ( rule__Neg__FAssignment_1 ) ) ;
    public final void rule__Neg__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1026:1: ( ( ( rule__Neg__FAssignment_1 ) ) )
            // InternalMyDsl.g:1027:1: ( ( rule__Neg__FAssignment_1 ) )
            {
            // InternalMyDsl.g:1027:1: ( ( rule__Neg__FAssignment_1 ) )
            // InternalMyDsl.g:1028:2: ( rule__Neg__FAssignment_1 )
            {
             before(grammarAccess.getNegAccess().getFAssignment_1()); 
            // InternalMyDsl.g:1029:2: ( rule__Neg__FAssignment_1 )
            // InternalMyDsl.g:1029:3: rule__Neg__FAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Neg__FAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getNegAccess().getFAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__Group__1__Impl"


    // $ANTLR start "rule__Binary__Group__0"
    // InternalMyDsl.g:1038:1: rule__Binary__Group__0 : rule__Binary__Group__0__Impl rule__Binary__Group__1 ;
    public final void rule__Binary__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1042:1: ( rule__Binary__Group__0__Impl rule__Binary__Group__1 )
            // InternalMyDsl.g:1043:2: rule__Binary__Group__0__Impl rule__Binary__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Binary__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binary__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__0"


    // $ANTLR start "rule__Binary__Group__0__Impl"
    // InternalMyDsl.g:1050:1: rule__Binary__Group__0__Impl : ( '(' ) ;
    public final void rule__Binary__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1054:1: ( ( '(' ) )
            // InternalMyDsl.g:1055:1: ( '(' )
            {
            // InternalMyDsl.g:1055:1: ( '(' )
            // InternalMyDsl.g:1056:2: '('
            {
             before(grammarAccess.getBinaryAccess().getLeftParenthesisKeyword_0()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getBinaryAccess().getLeftParenthesisKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__0__Impl"


    // $ANTLR start "rule__Binary__Group__1"
    // InternalMyDsl.g:1065:1: rule__Binary__Group__1 : rule__Binary__Group__1__Impl rule__Binary__Group__2 ;
    public final void rule__Binary__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1069:1: ( rule__Binary__Group__1__Impl rule__Binary__Group__2 )
            // InternalMyDsl.g:1070:2: rule__Binary__Group__1__Impl rule__Binary__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Binary__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binary__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__1"


    // $ANTLR start "rule__Binary__Group__1__Impl"
    // InternalMyDsl.g:1077:1: rule__Binary__Group__1__Impl : ( ( rule__Binary__LfAssignment_1 ) ) ;
    public final void rule__Binary__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1081:1: ( ( ( rule__Binary__LfAssignment_1 ) ) )
            // InternalMyDsl.g:1082:1: ( ( rule__Binary__LfAssignment_1 ) )
            {
            // InternalMyDsl.g:1082:1: ( ( rule__Binary__LfAssignment_1 ) )
            // InternalMyDsl.g:1083:2: ( rule__Binary__LfAssignment_1 )
            {
             before(grammarAccess.getBinaryAccess().getLfAssignment_1()); 
            // InternalMyDsl.g:1084:2: ( rule__Binary__LfAssignment_1 )
            // InternalMyDsl.g:1084:3: rule__Binary__LfAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Binary__LfAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getLfAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__1__Impl"


    // $ANTLR start "rule__Binary__Group__2"
    // InternalMyDsl.g:1092:1: rule__Binary__Group__2 : rule__Binary__Group__2__Impl rule__Binary__Group__3 ;
    public final void rule__Binary__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1096:1: ( rule__Binary__Group__2__Impl rule__Binary__Group__3 )
            // InternalMyDsl.g:1097:2: rule__Binary__Group__2__Impl rule__Binary__Group__3
            {
            pushFollow(FOLLOW_13);
            rule__Binary__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binary__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__2"


    // $ANTLR start "rule__Binary__Group__2__Impl"
    // InternalMyDsl.g:1104:1: rule__Binary__Group__2__Impl : ( ( rule__Binary__OpAssignment_2 ) ) ;
    public final void rule__Binary__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1108:1: ( ( ( rule__Binary__OpAssignment_2 ) ) )
            // InternalMyDsl.g:1109:1: ( ( rule__Binary__OpAssignment_2 ) )
            {
            // InternalMyDsl.g:1109:1: ( ( rule__Binary__OpAssignment_2 ) )
            // InternalMyDsl.g:1110:2: ( rule__Binary__OpAssignment_2 )
            {
             before(grammarAccess.getBinaryAccess().getOpAssignment_2()); 
            // InternalMyDsl.g:1111:2: ( rule__Binary__OpAssignment_2 )
            // InternalMyDsl.g:1111:3: rule__Binary__OpAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Binary__OpAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getOpAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__2__Impl"


    // $ANTLR start "rule__Binary__Group__3"
    // InternalMyDsl.g:1119:1: rule__Binary__Group__3 : rule__Binary__Group__3__Impl rule__Binary__Group__4 ;
    public final void rule__Binary__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1123:1: ( rule__Binary__Group__3__Impl rule__Binary__Group__4 )
            // InternalMyDsl.g:1124:2: rule__Binary__Group__3__Impl rule__Binary__Group__4
            {
            pushFollow(FOLLOW_15);
            rule__Binary__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Binary__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__3"


    // $ANTLR start "rule__Binary__Group__3__Impl"
    // InternalMyDsl.g:1131:1: rule__Binary__Group__3__Impl : ( ( rule__Binary__RfAssignment_3 ) ) ;
    public final void rule__Binary__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1135:1: ( ( ( rule__Binary__RfAssignment_3 ) ) )
            // InternalMyDsl.g:1136:1: ( ( rule__Binary__RfAssignment_3 ) )
            {
            // InternalMyDsl.g:1136:1: ( ( rule__Binary__RfAssignment_3 ) )
            // InternalMyDsl.g:1137:2: ( rule__Binary__RfAssignment_3 )
            {
             before(grammarAccess.getBinaryAccess().getRfAssignment_3()); 
            // InternalMyDsl.g:1138:2: ( rule__Binary__RfAssignment_3 )
            // InternalMyDsl.g:1138:3: rule__Binary__RfAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Binary__RfAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getBinaryAccess().getRfAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__3__Impl"


    // $ANTLR start "rule__Binary__Group__4"
    // InternalMyDsl.g:1146:1: rule__Binary__Group__4 : rule__Binary__Group__4__Impl ;
    public final void rule__Binary__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1150:1: ( rule__Binary__Group__4__Impl )
            // InternalMyDsl.g:1151:2: rule__Binary__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Binary__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__4"


    // $ANTLR start "rule__Binary__Group__4__Impl"
    // InternalMyDsl.g:1157:1: rule__Binary__Group__4__Impl : ( ')' ) ;
    public final void rule__Binary__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1161:1: ( ( ')' ) )
            // InternalMyDsl.g:1162:1: ( ')' )
            {
            // InternalMyDsl.g:1162:1: ( ')' )
            // InternalMyDsl.g:1163:2: ')'
            {
             before(grammarAccess.getBinaryAccess().getRightParenthesisKeyword_4()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getBinaryAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__Group__4__Impl"


    // $ANTLR start "rule__Root__NameAssignment_1"
    // InternalMyDsl.g:1173:1: rule__Root__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Root__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1177:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1178:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1178:2: ( RULE_ID )
            // InternalMyDsl.g:1179:3: RULE_ID
            {
             before(grammarAccess.getRootAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getRootAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__NameAssignment_1"


    // $ANTLR start "rule__Root__SonsAssignment_2_1"
    // InternalMyDsl.g:1188:1: rule__Root__SonsAssignment_2_1 : ( ruleNode ) ;
    public final void rule__Root__SonsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1192:1: ( ( ruleNode ) )
            // InternalMyDsl.g:1193:2: ( ruleNode )
            {
            // InternalMyDsl.g:1193:2: ( ruleNode )
            // InternalMyDsl.g:1194:3: ruleNode
            {
             before(grammarAccess.getRootAccess().getSonsNodeParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getRootAccess().getSonsNodeParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__SonsAssignment_2_1"


    // $ANTLR start "rule__Root__ConstraintAssignment_5"
    // InternalMyDsl.g:1203:1: rule__Root__ConstraintAssignment_5 : ( ruleFormula ) ;
    public final void rule__Root__ConstraintAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1207:1: ( ( ruleFormula ) )
            // InternalMyDsl.g:1208:2: ( ruleFormula )
            {
            // InternalMyDsl.g:1208:2: ( ruleFormula )
            // InternalMyDsl.g:1209:3: ruleFormula
            {
             before(grammarAccess.getRootAccess().getConstraintFormulaParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getRootAccess().getConstraintFormulaParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Root__ConstraintAssignment_5"


    // $ANTLR start "rule__UniNode__TypeAssignment_0"
    // InternalMyDsl.g:1218:1: rule__UniNode__TypeAssignment_0 : ( ruleNodeType ) ;
    public final void rule__UniNode__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1222:1: ( ( ruleNodeType ) )
            // InternalMyDsl.g:1223:2: ( ruleNodeType )
            {
            // InternalMyDsl.g:1223:2: ( ruleNodeType )
            // InternalMyDsl.g:1224:3: ruleNodeType
            {
             before(grammarAccess.getUniNodeAccess().getTypeNodeTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleNodeType();

            state._fsp--;

             after(grammarAccess.getUniNodeAccess().getTypeNodeTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__TypeAssignment_0"


    // $ANTLR start "rule__UniNode__NameAssignment_1"
    // InternalMyDsl.g:1233:1: rule__UniNode__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__UniNode__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1237:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1238:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1238:2: ( RULE_ID )
            // InternalMyDsl.g:1239:3: RULE_ID
            {
             before(grammarAccess.getUniNodeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getUniNodeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__NameAssignment_1"


    // $ANTLR start "rule__UniNode__SonsAssignment_2_1"
    // InternalMyDsl.g:1248:1: rule__UniNode__SonsAssignment_2_1 : ( ruleNode ) ;
    public final void rule__UniNode__SonsAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1252:1: ( ( ruleNode ) )
            // InternalMyDsl.g:1253:2: ( ruleNode )
            {
            // InternalMyDsl.g:1253:2: ( ruleNode )
            // InternalMyDsl.g:1254:3: ruleNode
            {
             before(grammarAccess.getUniNodeAccess().getSonsNodeParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getUniNodeAccess().getSonsNodeParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UniNode__SonsAssignment_2_1"


    // $ANTLR start "rule__MultiNode__TypeAssignment_0"
    // InternalMyDsl.g:1263:1: rule__MultiNode__TypeAssignment_0 : ( ruleMultiType ) ;
    public final void rule__MultiNode__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1267:1: ( ( ruleMultiType ) )
            // InternalMyDsl.g:1268:2: ( ruleMultiType )
            {
            // InternalMyDsl.g:1268:2: ( ruleMultiType )
            // InternalMyDsl.g:1269:3: ruleMultiType
            {
             before(grammarAccess.getMultiNodeAccess().getTypeMultiTypeParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiType();

            state._fsp--;

             after(grammarAccess.getMultiNodeAccess().getTypeMultiTypeParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__TypeAssignment_0"


    // $ANTLR start "rule__MultiNode__SonsAssignment_2"
    // InternalMyDsl.g:1278:1: rule__MultiNode__SonsAssignment_2 : ( ruleNode ) ;
    public final void rule__MultiNode__SonsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1282:1: ( ( ruleNode ) )
            // InternalMyDsl.g:1283:2: ( ruleNode )
            {
            // InternalMyDsl.g:1283:2: ( ruleNode )
            // InternalMyDsl.g:1284:3: ruleNode
            {
             before(grammarAccess.getMultiNodeAccess().getSonsNodeParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleNode();

            state._fsp--;

             after(grammarAccess.getMultiNodeAccess().getSonsNodeParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiNode__SonsAssignment_2"


    // $ANTLR start "rule__Formula__FeatureAssignment_0"
    // InternalMyDsl.g:1293:1: rule__Formula__FeatureAssignment_0 : ( RULE_ID ) ;
    public final void rule__Formula__FeatureAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1297:1: ( ( RULE_ID ) )
            // InternalMyDsl.g:1298:2: ( RULE_ID )
            {
            // InternalMyDsl.g:1298:2: ( RULE_ID )
            // InternalMyDsl.g:1299:3: RULE_ID
            {
             before(grammarAccess.getFormulaAccess().getFeatureIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFormulaAccess().getFeatureIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__FeatureAssignment_0"


    // $ANTLR start "rule__Neg__FAssignment_1"
    // InternalMyDsl.g:1308:1: rule__Neg__FAssignment_1 : ( ruleFormula ) ;
    public final void rule__Neg__FAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1312:1: ( ( ruleFormula ) )
            // InternalMyDsl.g:1313:2: ( ruleFormula )
            {
            // InternalMyDsl.g:1313:2: ( ruleFormula )
            // InternalMyDsl.g:1314:3: ruleFormula
            {
             before(grammarAccess.getNegAccess().getFFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getNegAccess().getFFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Neg__FAssignment_1"


    // $ANTLR start "rule__Binary__LfAssignment_1"
    // InternalMyDsl.g:1323:1: rule__Binary__LfAssignment_1 : ( ruleFormula ) ;
    public final void rule__Binary__LfAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1327:1: ( ( ruleFormula ) )
            // InternalMyDsl.g:1328:2: ( ruleFormula )
            {
            // InternalMyDsl.g:1328:2: ( ruleFormula )
            // InternalMyDsl.g:1329:3: ruleFormula
            {
             before(grammarAccess.getBinaryAccess().getLfFormulaParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getBinaryAccess().getLfFormulaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__LfAssignment_1"


    // $ANTLR start "rule__Binary__OpAssignment_2"
    // InternalMyDsl.g:1338:1: rule__Binary__OpAssignment_2 : ( ruleOp ) ;
    public final void rule__Binary__OpAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1342:1: ( ( ruleOp ) )
            // InternalMyDsl.g:1343:2: ( ruleOp )
            {
            // InternalMyDsl.g:1343:2: ( ruleOp )
            // InternalMyDsl.g:1344:3: ruleOp
            {
             before(grammarAccess.getBinaryAccess().getOpOpParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleOp();

            state._fsp--;

             after(grammarAccess.getBinaryAccess().getOpOpParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__OpAssignment_2"


    // $ANTLR start "rule__Binary__RfAssignment_3"
    // InternalMyDsl.g:1353:1: rule__Binary__RfAssignment_3 : ( ruleFormula ) ;
    public final void rule__Binary__RfAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1357:1: ( ( ruleFormula ) )
            // InternalMyDsl.g:1358:2: ( ruleFormula )
            {
            // InternalMyDsl.g:1358:2: ( ruleFormula )
            // InternalMyDsl.g:1359:3: ruleFormula
            {
             before(grammarAccess.getBinaryAccess().getRfFormulaParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getBinaryAccess().getRfFormulaParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Binary__RfAssignment_3"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000019000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000018000012L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000401F800L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000001F802L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x000000000001F800L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000018000010L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000000001E0000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000020000000L});

}