import java.util.HashMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.xtext.example.mydsl.myDsl.MyDslFactory;
import org.xtext.example.mydsl.myDsl.Node;
import org.xtext.example.mydsl.myDsl.Root;
import org.xtext.example.mydsl.myDsl.UniNode;
import org.xtext.example.mydsl.myDsl.impl.MyDslFactoryImpl;

@SuppressWarnings("all")
public class test {
  public static void main(final String[] args) {
    try {
      final MyDslFactory factory = MyDslFactoryImpl.init();
      final Root root = factory.createRoot();
      root.setName("Banana");
      final EList<Node> sons = root.getSons();
      final UniNode node = factory.createUniNode();
      node.setName("Bananana");
      sons.add(node);
      Resource rs = new ResourceSetImpl().createResource(URI.createURI("foo1.bfm"));
      EList<EObject> content = rs.getContents();
      content.add(root);
      HashMap<Object, Object> _hashMap = new HashMap<Object, Object>();
      rs.save(_hashMap);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
