import org.xtext.example.mydsl.myDsl.impl.MyDslFactoryImpl;
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import java.util.HashMap

class test {
    def static void main(String[] args) {
    	// Creation of a feature model
    	val factory = MyDslFactoryImpl.init()
		val root = factory.createRoot()
		root.setName("Banana")
		val sons = root.getSons()
		val node = factory.createUniNode()
		node.setName("Bananana")
		sons.add( node )

		// Save of the feature model
		var Resource rs = new ResourceSetImpl().createResource(URI.createURI("foo1.bfm"));
		var content = rs.getContents();
		content.add(root)
		rs.save(new HashMap());
    }
}