package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Root'", "'{'", "'}'", "'Constraints'", "'['", "']'", "'Mandatory'", "'Optional'", "'Xor'", "'Or'", "'OrGroup'", "'XorGroup'", "'~'", "'('", "')'", "'or'", "'and'", "'=>'", "'<=>'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=5;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Root";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleRoot"
    // InternalMyDsl.g:64:1: entryRuleRoot returns [EObject current=null] : iv_ruleRoot= ruleRoot EOF ;
    public final EObject entryRuleRoot() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoot = null;


        try {
            // InternalMyDsl.g:64:45: (iv_ruleRoot= ruleRoot EOF )
            // InternalMyDsl.g:65:2: iv_ruleRoot= ruleRoot EOF
            {
             newCompositeNode(grammarAccess.getRootRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRoot=ruleRoot();

            state._fsp--;

             current =iv_ruleRoot; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoot"


    // $ANTLR start "ruleRoot"
    // InternalMyDsl.g:71:1: ruleRoot returns [EObject current=null] : (otherlv_0= 'Root' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? otherlv_5= 'Constraints' otherlv_6= '[' ( (lv_constraint_7_0= ruleFormula ) )* otherlv_8= ']' ) ;
    public final EObject ruleRoot() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_sons_3_0 = null;

        EObject lv_constraint_7_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( (otherlv_0= 'Root' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? otherlv_5= 'Constraints' otherlv_6= '[' ( (lv_constraint_7_0= ruleFormula ) )* otherlv_8= ']' ) )
            // InternalMyDsl.g:78:2: (otherlv_0= 'Root' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? otherlv_5= 'Constraints' otherlv_6= '[' ( (lv_constraint_7_0= ruleFormula ) )* otherlv_8= ']' )
            {
            // InternalMyDsl.g:78:2: (otherlv_0= 'Root' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? otherlv_5= 'Constraints' otherlv_6= '[' ( (lv_constraint_7_0= ruleFormula ) )* otherlv_8= ']' )
            // InternalMyDsl.g:79:3: otherlv_0= 'Root' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? otherlv_5= 'Constraints' otherlv_6= '[' ( (lv_constraint_7_0= ruleFormula ) )* otherlv_8= ']'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getRootAccess().getRootKeyword_0());
            		
            // InternalMyDsl.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalMyDsl.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalMyDsl.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalMyDsl.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getRootAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getRootRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalMyDsl.g:101:3: (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:102:4: otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}'
                    {
                    otherlv_2=(Token)match(input,12,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getRootAccess().getLeftCurlyBracketKeyword_2_0());
                    			
                    // InternalMyDsl.g:106:4: ( (lv_sons_3_0= ruleNode ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( ((LA1_0>=17 && LA1_0<=22)) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalMyDsl.g:107:5: (lv_sons_3_0= ruleNode )
                    	    {
                    	    // InternalMyDsl.g:107:5: (lv_sons_3_0= ruleNode )
                    	    // InternalMyDsl.g:108:6: lv_sons_3_0= ruleNode
                    	    {

                    	    						newCompositeNode(grammarAccess.getRootAccess().getSonsNodeParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_5);
                    	    lv_sons_3_0=ruleNode();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRootRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"sons",
                    	    							lv_sons_3_0,
                    	    							"org.xtext.example.mydsl.MyDsl.Node");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,13,FOLLOW_6); 

                    				newLeafNode(otherlv_4, grammarAccess.getRootAccess().getRightCurlyBracketKeyword_2_2());
                    			

                    }
                    break;

            }

            otherlv_5=(Token)match(input,14,FOLLOW_7); 

            			newLeafNode(otherlv_5, grammarAccess.getRootAccess().getConstraintsKeyword_3());
            		
            otherlv_6=(Token)match(input,15,FOLLOW_8); 

            			newLeafNode(otherlv_6, grammarAccess.getRootAccess().getLeftSquareBracketKeyword_4());
            		
            // InternalMyDsl.g:138:3: ( (lv_constraint_7_0= ruleFormula ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_ID||(LA3_0>=23 && LA3_0<=24)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:139:4: (lv_constraint_7_0= ruleFormula )
            	    {
            	    // InternalMyDsl.g:139:4: (lv_constraint_7_0= ruleFormula )
            	    // InternalMyDsl.g:140:5: lv_constraint_7_0= ruleFormula
            	    {

            	    					newCompositeNode(grammarAccess.getRootAccess().getConstraintFormulaParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_constraint_7_0=ruleFormula();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getRootRule());
            	    					}
            	    					add(
            	    						current,
            	    						"constraint",
            	    						lv_constraint_7_0,
            	    						"org.xtext.example.mydsl.MyDsl.Formula");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            otherlv_8=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getRootAccess().getRightSquareBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoot"


    // $ANTLR start "entryRuleNode"
    // InternalMyDsl.g:165:1: entryRuleNode returns [EObject current=null] : iv_ruleNode= ruleNode EOF ;
    public final EObject entryRuleNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNode = null;


        try {
            // InternalMyDsl.g:165:45: (iv_ruleNode= ruleNode EOF )
            // InternalMyDsl.g:166:2: iv_ruleNode= ruleNode EOF
            {
             newCompositeNode(grammarAccess.getNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNode=ruleNode();

            state._fsp--;

             current =iv_ruleNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNode"


    // $ANTLR start "ruleNode"
    // InternalMyDsl.g:172:1: ruleNode returns [EObject current=null] : (this_UniNode_0= ruleUniNode | this_MultiNode_1= ruleMultiNode ) ;
    public final EObject ruleNode() throws RecognitionException {
        EObject current = null;

        EObject this_UniNode_0 = null;

        EObject this_MultiNode_1 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:178:2: ( (this_UniNode_0= ruleUniNode | this_MultiNode_1= ruleMultiNode ) )
            // InternalMyDsl.g:179:2: (this_UniNode_0= ruleUniNode | this_MultiNode_1= ruleMultiNode )
            {
            // InternalMyDsl.g:179:2: (this_UniNode_0= ruleUniNode | this_MultiNode_1= ruleMultiNode )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( ((LA4_0>=17 && LA4_0<=20)) ) {
                alt4=1;
            }
            else if ( ((LA4_0>=21 && LA4_0<=22)) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:180:3: this_UniNode_0= ruleUniNode
                    {

                    			newCompositeNode(grammarAccess.getNodeAccess().getUniNodeParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_UniNode_0=ruleUniNode();

                    state._fsp--;


                    			current = this_UniNode_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:189:3: this_MultiNode_1= ruleMultiNode
                    {

                    			newCompositeNode(grammarAccess.getNodeAccess().getMultiNodeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_MultiNode_1=ruleMultiNode();

                    state._fsp--;


                    			current = this_MultiNode_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNode"


    // $ANTLR start "entryRuleUniNode"
    // InternalMyDsl.g:201:1: entryRuleUniNode returns [EObject current=null] : iv_ruleUniNode= ruleUniNode EOF ;
    public final EObject entryRuleUniNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUniNode = null;


        try {
            // InternalMyDsl.g:201:48: (iv_ruleUniNode= ruleUniNode EOF )
            // InternalMyDsl.g:202:2: iv_ruleUniNode= ruleUniNode EOF
            {
             newCompositeNode(grammarAccess.getUniNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUniNode=ruleUniNode();

            state._fsp--;

             current =iv_ruleUniNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUniNode"


    // $ANTLR start "ruleUniNode"
    // InternalMyDsl.g:208:1: ruleUniNode returns [EObject current=null] : ( ( (lv_type_0_0= ruleNodeType ) ) ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? ) ;
    public final EObject ruleUniNode() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_type_0_0 = null;

        EObject lv_sons_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:214:2: ( ( ( (lv_type_0_0= ruleNodeType ) ) ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? ) )
            // InternalMyDsl.g:215:2: ( ( (lv_type_0_0= ruleNodeType ) ) ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? )
            {
            // InternalMyDsl.g:215:2: ( ( (lv_type_0_0= ruleNodeType ) ) ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )? )
            // InternalMyDsl.g:216:3: ( (lv_type_0_0= ruleNodeType ) ) ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )?
            {
            // InternalMyDsl.g:216:3: ( (lv_type_0_0= ruleNodeType ) )
            // InternalMyDsl.g:217:4: (lv_type_0_0= ruleNodeType )
            {
            // InternalMyDsl.g:217:4: (lv_type_0_0= ruleNodeType )
            // InternalMyDsl.g:218:5: lv_type_0_0= ruleNodeType
            {

            					newCompositeNode(grammarAccess.getUniNodeAccess().getTypeNodeTypeParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_3);
            lv_type_0_0=ruleNodeType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUniNodeRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_0_0,
            						"org.xtext.example.mydsl.MyDsl.NodeType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:235:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalMyDsl.g:236:4: (lv_name_1_0= RULE_ID )
            {
            // InternalMyDsl.g:236:4: (lv_name_1_0= RULE_ID )
            // InternalMyDsl.g:237:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getUniNodeAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getUniNodeRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalMyDsl.g:253:3: (otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==12) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:254:4: otherlv_2= '{' ( (lv_sons_3_0= ruleNode ) )* otherlv_4= '}'
                    {
                    otherlv_2=(Token)match(input,12,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getUniNodeAccess().getLeftCurlyBracketKeyword_2_0());
                    			
                    // InternalMyDsl.g:258:4: ( (lv_sons_3_0= ruleNode ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( ((LA5_0>=17 && LA5_0<=22)) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalMyDsl.g:259:5: (lv_sons_3_0= ruleNode )
                    	    {
                    	    // InternalMyDsl.g:259:5: (lv_sons_3_0= ruleNode )
                    	    // InternalMyDsl.g:260:6: lv_sons_3_0= ruleNode
                    	    {

                    	    						newCompositeNode(grammarAccess.getUniNodeAccess().getSonsNodeParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_5);
                    	    lv_sons_3_0=ruleNode();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getUniNodeRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"sons",
                    	    							lv_sons_3_0,
                    	    							"org.xtext.example.mydsl.MyDsl.Node");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,13,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getUniNodeAccess().getRightCurlyBracketKeyword_2_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUniNode"


    // $ANTLR start "entryRuleNodeType"
    // InternalMyDsl.g:286:1: entryRuleNodeType returns [String current=null] : iv_ruleNodeType= ruleNodeType EOF ;
    public final String entryRuleNodeType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleNodeType = null;


        try {
            // InternalMyDsl.g:286:48: (iv_ruleNodeType= ruleNodeType EOF )
            // InternalMyDsl.g:287:2: iv_ruleNodeType= ruleNodeType EOF
            {
             newCompositeNode(grammarAccess.getNodeTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNodeType=ruleNodeType();

            state._fsp--;

             current =iv_ruleNodeType.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNodeType"


    // $ANTLR start "ruleNodeType"
    // InternalMyDsl.g:293:1: ruleNodeType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'Mandatory' | kw= 'Optional' | kw= 'Xor' | kw= 'Or' ) ;
    public final AntlrDatatypeRuleToken ruleNodeType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMyDsl.g:299:2: ( (kw= 'Mandatory' | kw= 'Optional' | kw= 'Xor' | kw= 'Or' ) )
            // InternalMyDsl.g:300:2: (kw= 'Mandatory' | kw= 'Optional' | kw= 'Xor' | kw= 'Or' )
            {
            // InternalMyDsl.g:300:2: (kw= 'Mandatory' | kw= 'Optional' | kw= 'Xor' | kw= 'Or' )
            int alt7=4;
            switch ( input.LA(1) ) {
            case 17:
                {
                alt7=1;
                }
                break;
            case 18:
                {
                alt7=2;
                }
                break;
            case 19:
                {
                alt7=3;
                }
                break;
            case 20:
                {
                alt7=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalMyDsl.g:301:3: kw= 'Mandatory'
                    {
                    kw=(Token)match(input,17,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeTypeAccess().getMandatoryKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:307:3: kw= 'Optional'
                    {
                    kw=(Token)match(input,18,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeTypeAccess().getOptionalKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:313:3: kw= 'Xor'
                    {
                    kw=(Token)match(input,19,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeTypeAccess().getXorKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:319:3: kw= 'Or'
                    {
                    kw=(Token)match(input,20,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getNodeTypeAccess().getOrKeyword_3());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNodeType"


    // $ANTLR start "entryRuleMultiNode"
    // InternalMyDsl.g:328:1: entryRuleMultiNode returns [EObject current=null] : iv_ruleMultiNode= ruleMultiNode EOF ;
    public final EObject entryRuleMultiNode() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiNode = null;


        try {
            // InternalMyDsl.g:328:50: (iv_ruleMultiNode= ruleMultiNode EOF )
            // InternalMyDsl.g:329:2: iv_ruleMultiNode= ruleMultiNode EOF
            {
             newCompositeNode(grammarAccess.getMultiNodeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultiNode=ruleMultiNode();

            state._fsp--;

             current =iv_ruleMultiNode; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiNode"


    // $ANTLR start "ruleMultiNode"
    // InternalMyDsl.g:335:1: ruleMultiNode returns [EObject current=null] : ( ( (lv_type_0_0= ruleMultiType ) ) otherlv_1= '{' ( (lv_sons_2_0= ruleNode ) )+ otherlv_3= '}' ) ;
    public final EObject ruleMultiNode() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_type_0_0 = null;

        EObject lv_sons_2_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:341:2: ( ( ( (lv_type_0_0= ruleMultiType ) ) otherlv_1= '{' ( (lv_sons_2_0= ruleNode ) )+ otherlv_3= '}' ) )
            // InternalMyDsl.g:342:2: ( ( (lv_type_0_0= ruleMultiType ) ) otherlv_1= '{' ( (lv_sons_2_0= ruleNode ) )+ otherlv_3= '}' )
            {
            // InternalMyDsl.g:342:2: ( ( (lv_type_0_0= ruleMultiType ) ) otherlv_1= '{' ( (lv_sons_2_0= ruleNode ) )+ otherlv_3= '}' )
            // InternalMyDsl.g:343:3: ( (lv_type_0_0= ruleMultiType ) ) otherlv_1= '{' ( (lv_sons_2_0= ruleNode ) )+ otherlv_3= '}'
            {
            // InternalMyDsl.g:343:3: ( (lv_type_0_0= ruleMultiType ) )
            // InternalMyDsl.g:344:4: (lv_type_0_0= ruleMultiType )
            {
            // InternalMyDsl.g:344:4: (lv_type_0_0= ruleMultiType )
            // InternalMyDsl.g:345:5: lv_type_0_0= ruleMultiType
            {

            					newCompositeNode(grammarAccess.getMultiNodeAccess().getTypeMultiTypeParserRuleCall_0_0());
            				
            pushFollow(FOLLOW_10);
            lv_type_0_0=ruleMultiType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getMultiNodeRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_0_0,
            						"org.xtext.example.mydsl.MyDsl.MultiType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_1=(Token)match(input,12,FOLLOW_11); 

            			newLeafNode(otherlv_1, grammarAccess.getMultiNodeAccess().getLeftCurlyBracketKeyword_1());
            		
            // InternalMyDsl.g:366:3: ( (lv_sons_2_0= ruleNode ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=17 && LA8_0<=22)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:367:4: (lv_sons_2_0= ruleNode )
            	    {
            	    // InternalMyDsl.g:367:4: (lv_sons_2_0= ruleNode )
            	    // InternalMyDsl.g:368:5: lv_sons_2_0= ruleNode
            	    {

            	    					newCompositeNode(grammarAccess.getMultiNodeAccess().getSonsNodeParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_5);
            	    lv_sons_2_0=ruleNode();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getMultiNodeRule());
            	    					}
            	    					add(
            	    						current,
            	    						"sons",
            	    						lv_sons_2_0,
            	    						"org.xtext.example.mydsl.MyDsl.Node");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);

            otherlv_3=(Token)match(input,13,FOLLOW_2); 

            			newLeafNode(otherlv_3, grammarAccess.getMultiNodeAccess().getRightCurlyBracketKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiNode"


    // $ANTLR start "entryRuleMultiType"
    // InternalMyDsl.g:393:1: entryRuleMultiType returns [String current=null] : iv_ruleMultiType= ruleMultiType EOF ;
    public final String entryRuleMultiType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMultiType = null;


        try {
            // InternalMyDsl.g:393:49: (iv_ruleMultiType= ruleMultiType EOF )
            // InternalMyDsl.g:394:2: iv_ruleMultiType= ruleMultiType EOF
            {
             newCompositeNode(grammarAccess.getMultiTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultiType=ruleMultiType();

            state._fsp--;

             current =iv_ruleMultiType.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiType"


    // $ANTLR start "ruleMultiType"
    // InternalMyDsl.g:400:1: ruleMultiType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'OrGroup' | kw= 'XorGroup' ) ;
    public final AntlrDatatypeRuleToken ruleMultiType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMyDsl.g:406:2: ( (kw= 'OrGroup' | kw= 'XorGroup' ) )
            // InternalMyDsl.g:407:2: (kw= 'OrGroup' | kw= 'XorGroup' )
            {
            // InternalMyDsl.g:407:2: (kw= 'OrGroup' | kw= 'XorGroup' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==21) ) {
                alt9=1;
            }
            else if ( (LA9_0==22) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalMyDsl.g:408:3: kw= 'OrGroup'
                    {
                    kw=(Token)match(input,21,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getMultiTypeAccess().getOrGroupKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:414:3: kw= 'XorGroup'
                    {
                    kw=(Token)match(input,22,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getMultiTypeAccess().getXorGroupKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiType"


    // $ANTLR start "entryRuleFormula"
    // InternalMyDsl.g:423:1: entryRuleFormula returns [EObject current=null] : iv_ruleFormula= ruleFormula EOF ;
    public final EObject entryRuleFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormula = null;


        try {
            // InternalMyDsl.g:423:48: (iv_ruleFormula= ruleFormula EOF )
            // InternalMyDsl.g:424:2: iv_ruleFormula= ruleFormula EOF
            {
             newCompositeNode(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFormula=ruleFormula();

            state._fsp--;

             current =iv_ruleFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalMyDsl.g:430:1: ruleFormula returns [EObject current=null] : ( ( (lv_feature_0_0= RULE_ID ) ) | this_Neg_1= ruleNeg | this_Binary_2= ruleBinary ) ;
    public final EObject ruleFormula() throws RecognitionException {
        EObject current = null;

        Token lv_feature_0_0=null;
        EObject this_Neg_1 = null;

        EObject this_Binary_2 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:436:2: ( ( ( (lv_feature_0_0= RULE_ID ) ) | this_Neg_1= ruleNeg | this_Binary_2= ruleBinary ) )
            // InternalMyDsl.g:437:2: ( ( (lv_feature_0_0= RULE_ID ) ) | this_Neg_1= ruleNeg | this_Binary_2= ruleBinary )
            {
            // InternalMyDsl.g:437:2: ( ( (lv_feature_0_0= RULE_ID ) ) | this_Neg_1= ruleNeg | this_Binary_2= ruleBinary )
            int alt10=3;
            switch ( input.LA(1) ) {
            case RULE_ID:
                {
                alt10=1;
                }
                break;
            case 23:
                {
                alt10=2;
                }
                break;
            case 24:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalMyDsl.g:438:3: ( (lv_feature_0_0= RULE_ID ) )
                    {
                    // InternalMyDsl.g:438:3: ( (lv_feature_0_0= RULE_ID ) )
                    // InternalMyDsl.g:439:4: (lv_feature_0_0= RULE_ID )
                    {
                    // InternalMyDsl.g:439:4: (lv_feature_0_0= RULE_ID )
                    // InternalMyDsl.g:440:5: lv_feature_0_0= RULE_ID
                    {
                    lv_feature_0_0=(Token)match(input,RULE_ID,FOLLOW_2); 

                    					newLeafNode(lv_feature_0_0, grammarAccess.getFormulaAccess().getFeatureIDTerminalRuleCall_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getFormulaRule());
                    					}
                    					setWithLastConsumed(
                    						current,
                    						"feature",
                    						lv_feature_0_0,
                    						"org.eclipse.xtext.common.Terminals.ID");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:457:3: this_Neg_1= ruleNeg
                    {

                    			newCompositeNode(grammarAccess.getFormulaAccess().getNegParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Neg_1=ruleNeg();

                    state._fsp--;


                    			current = this_Neg_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:466:3: this_Binary_2= ruleBinary
                    {

                    			newCompositeNode(grammarAccess.getFormulaAccess().getBinaryParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Binary_2=ruleBinary();

                    state._fsp--;


                    			current = this_Binary_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleNeg"
    // InternalMyDsl.g:478:1: entryRuleNeg returns [EObject current=null] : iv_ruleNeg= ruleNeg EOF ;
    public final EObject entryRuleNeg() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNeg = null;


        try {
            // InternalMyDsl.g:478:44: (iv_ruleNeg= ruleNeg EOF )
            // InternalMyDsl.g:479:2: iv_ruleNeg= ruleNeg EOF
            {
             newCompositeNode(grammarAccess.getNegRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNeg=ruleNeg();

            state._fsp--;

             current =iv_ruleNeg; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNeg"


    // $ANTLR start "ruleNeg"
    // InternalMyDsl.g:485:1: ruleNeg returns [EObject current=null] : (otherlv_0= '~' ( (lv_f_1_0= ruleFormula ) ) ) ;
    public final EObject ruleNeg() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_f_1_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:491:2: ( (otherlv_0= '~' ( (lv_f_1_0= ruleFormula ) ) ) )
            // InternalMyDsl.g:492:2: (otherlv_0= '~' ( (lv_f_1_0= ruleFormula ) ) )
            {
            // InternalMyDsl.g:492:2: (otherlv_0= '~' ( (lv_f_1_0= ruleFormula ) ) )
            // InternalMyDsl.g:493:3: otherlv_0= '~' ( (lv_f_1_0= ruleFormula ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getNegAccess().getTildeKeyword_0());
            		
            // InternalMyDsl.g:497:3: ( (lv_f_1_0= ruleFormula ) )
            // InternalMyDsl.g:498:4: (lv_f_1_0= ruleFormula )
            {
            // InternalMyDsl.g:498:4: (lv_f_1_0= ruleFormula )
            // InternalMyDsl.g:499:5: lv_f_1_0= ruleFormula
            {

            					newCompositeNode(grammarAccess.getNegAccess().getFFormulaParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_f_1_0=ruleFormula();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getNegRule());
            					}
            					set(
            						current,
            						"f",
            						lv_f_1_0,
            						"org.xtext.example.mydsl.MyDsl.Formula");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNeg"


    // $ANTLR start "entryRuleBinary"
    // InternalMyDsl.g:520:1: entryRuleBinary returns [EObject current=null] : iv_ruleBinary= ruleBinary EOF ;
    public final EObject entryRuleBinary() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBinary = null;


        try {
            // InternalMyDsl.g:520:47: (iv_ruleBinary= ruleBinary EOF )
            // InternalMyDsl.g:521:2: iv_ruleBinary= ruleBinary EOF
            {
             newCompositeNode(grammarAccess.getBinaryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBinary=ruleBinary();

            state._fsp--;

             current =iv_ruleBinary; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBinary"


    // $ANTLR start "ruleBinary"
    // InternalMyDsl.g:527:1: ruleBinary returns [EObject current=null] : (otherlv_0= '(' ( (lv_lf_1_0= ruleFormula ) ) ( (lv_op_2_0= ruleOp ) ) ( (lv_rf_3_0= ruleFormula ) ) otherlv_4= ')' ) ;
    public final EObject ruleBinary() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_4=null;
        EObject lv_lf_1_0 = null;

        AntlrDatatypeRuleToken lv_op_2_0 = null;

        EObject lv_rf_3_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:533:2: ( (otherlv_0= '(' ( (lv_lf_1_0= ruleFormula ) ) ( (lv_op_2_0= ruleOp ) ) ( (lv_rf_3_0= ruleFormula ) ) otherlv_4= ')' ) )
            // InternalMyDsl.g:534:2: (otherlv_0= '(' ( (lv_lf_1_0= ruleFormula ) ) ( (lv_op_2_0= ruleOp ) ) ( (lv_rf_3_0= ruleFormula ) ) otherlv_4= ')' )
            {
            // InternalMyDsl.g:534:2: (otherlv_0= '(' ( (lv_lf_1_0= ruleFormula ) ) ( (lv_op_2_0= ruleOp ) ) ( (lv_rf_3_0= ruleFormula ) ) otherlv_4= ')' )
            // InternalMyDsl.g:535:3: otherlv_0= '(' ( (lv_lf_1_0= ruleFormula ) ) ( (lv_op_2_0= ruleOp ) ) ( (lv_rf_3_0= ruleFormula ) ) otherlv_4= ')'
            {
            otherlv_0=(Token)match(input,24,FOLLOW_12); 

            			newLeafNode(otherlv_0, grammarAccess.getBinaryAccess().getLeftParenthesisKeyword_0());
            		
            // InternalMyDsl.g:539:3: ( (lv_lf_1_0= ruleFormula ) )
            // InternalMyDsl.g:540:4: (lv_lf_1_0= ruleFormula )
            {
            // InternalMyDsl.g:540:4: (lv_lf_1_0= ruleFormula )
            // InternalMyDsl.g:541:5: lv_lf_1_0= ruleFormula
            {

            					newCompositeNode(grammarAccess.getBinaryAccess().getLfFormulaParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_13);
            lv_lf_1_0=ruleFormula();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinaryRule());
            					}
            					set(
            						current,
            						"lf",
            						lv_lf_1_0,
            						"org.xtext.example.mydsl.MyDsl.Formula");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:558:3: ( (lv_op_2_0= ruleOp ) )
            // InternalMyDsl.g:559:4: (lv_op_2_0= ruleOp )
            {
            // InternalMyDsl.g:559:4: (lv_op_2_0= ruleOp )
            // InternalMyDsl.g:560:5: lv_op_2_0= ruleOp
            {

            					newCompositeNode(grammarAccess.getBinaryAccess().getOpOpParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_12);
            lv_op_2_0=ruleOp();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinaryRule());
            					}
            					set(
            						current,
            						"op",
            						lv_op_2_0,
            						"org.xtext.example.mydsl.MyDsl.Op");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalMyDsl.g:577:3: ( (lv_rf_3_0= ruleFormula ) )
            // InternalMyDsl.g:578:4: (lv_rf_3_0= ruleFormula )
            {
            // InternalMyDsl.g:578:4: (lv_rf_3_0= ruleFormula )
            // InternalMyDsl.g:579:5: lv_rf_3_0= ruleFormula
            {

            					newCompositeNode(grammarAccess.getBinaryAccess().getRfFormulaParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_14);
            lv_rf_3_0=ruleFormula();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getBinaryRule());
            					}
            					set(
            						current,
            						"rf",
            						lv_rf_3_0,
            						"org.xtext.example.mydsl.MyDsl.Formula");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,25,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getBinaryAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBinary"


    // $ANTLR start "entryRuleOp"
    // InternalMyDsl.g:604:1: entryRuleOp returns [String current=null] : iv_ruleOp= ruleOp EOF ;
    public final String entryRuleOp() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOp = null;


        try {
            // InternalMyDsl.g:604:42: (iv_ruleOp= ruleOp EOF )
            // InternalMyDsl.g:605:2: iv_ruleOp= ruleOp EOF
            {
             newCompositeNode(grammarAccess.getOpRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOp=ruleOp();

            state._fsp--;

             current =iv_ruleOp.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOp"


    // $ANTLR start "ruleOp"
    // InternalMyDsl.g:611:1: ruleOp returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'or' | kw= 'and' | kw= '=>' | kw= '<=>' ) ;
    public final AntlrDatatypeRuleToken ruleOp() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalMyDsl.g:617:2: ( (kw= 'or' | kw= 'and' | kw= '=>' | kw= '<=>' ) )
            // InternalMyDsl.g:618:2: (kw= 'or' | kw= 'and' | kw= '=>' | kw= '<=>' )
            {
            // InternalMyDsl.g:618:2: (kw= 'or' | kw= 'and' | kw= '=>' | kw= '<=>' )
            int alt11=4;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt11=1;
                }
                break;
            case 27:
                {
                alt11=2;
                }
                break;
            case 28:
                {
                alt11=3;
                }
                break;
            case 29:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalMyDsl.g:619:3: kw= 'or'
                    {
                    kw=(Token)match(input,26,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getOrKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:625:3: kw= 'and'
                    {
                    kw=(Token)match(input,27,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getAndKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:631:3: kw= '=>'
                    {
                    kw=(Token)match(input,28,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getEqualsSignGreaterThanSignKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:637:3: kw= '<=>'
                    {
                    kw=(Token)match(input,29,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getOpAccess().getLessThanSignEqualsSignGreaterThanSignKeyword_3());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOp"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000005000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x00000000007E2000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000001810010L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000007E0000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001800010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x000000003C000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000002000000L});

}